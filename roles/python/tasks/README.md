# Installation

## Ansible

    sudo apt update
    sudo apt install software-properties-common
    sudo apt-add-repository --yes --update ppa:ansible/ansible
    sudo apt install ansible
    
Si nécessaire, voir les [autres méthodes d'installation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
    
## Nœud ELK/Duniter

1. DigitalOcean : droplet
    * Droplet Standard $20/mois $0.03/heure (4GB 2CPUs 80GB SSD 4TB transfert)
    * Frankfurt
    * Clés SSH

2. Copier/coller l'IP de la droplet dans hosts.yml

3. Lancer la commande :

       ansible-playbook site.yml
       
4. Attendre environ 40'

5. Accéder à Kibana : `http://<ip>:5601`

## Configuration des Index Patterns

### MetricBeat

Dans Kibana > Management > Index Patterns > Create index pattern : 

* saisir `metricbeat-*` puis cliquer sur « Next step »
* choisir `@timestamp` puis cliquer sur « Create index pattern »

Aller dans Kibana > Visualize pour observer la liste des données récoltées à travers le temps.