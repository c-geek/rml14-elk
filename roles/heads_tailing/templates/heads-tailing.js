const client = new (require('websocket').client);

client.on('connect', (c) => {
  c.on('message', (m) => {
    const heads = JSON.parse(m.utf8Data);
    // Dump the heads line by line, with only the wanted fields
    heads.forEach(h => console.log(JSON.stringify({
      messageV2: h.messageV2,
      step: h.step,
      ts: new Date().toISOString()
    })))
  });
  c.on('error', () => process.exit(1) );
});

client.connect('ws://46.101.141.120:9220/ws/heads');